<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateExhibitorList extends Controller
{
    public function filter(){
        
        $activitiesHTML = '';
        $activities = file_get_contents(env('ACTIVITIES'));
        $activities = json_decode($activities, true);

        $i = 1;
        
        foreach($activities as $activity){
            if($activity['count'] > 0){
                $name = $activity['name'];
                $id = $activity['id'];
                if( strpos( $name, '&amp;' ) !== false){
                    $name = str_replace('&amp;', '&', $name);
                }

                $activitiesHTML .= '<div class="col-6 col-sm-4 col-lg-3"><input type="checkbox" name="check[]" value="'.$id.'" class="filter-check"> '.$name.'</div>';
            }
        }

        return $activitiesHTML;
    }

    public function exhibitors(){
        $exhibitors = file_get_contents(env('EXHIBITORS'));
        $exhibitors = json_decode($exhibitors, true);
        // Sort by Title
        usort($exhibitors, function($a, $b) {
            return strtolower($a['title']['rendered']) <=> strtolower($b['title']['rendered']);
        });

        $exhibitorHTML = '';

        $exI = 1;
        $currentLetter = "";
        foreach($exhibitors as $exhibitor){
            if(!is_string($exhibitor['acf']['show'])){
                if(in_array('csha', $exhibitor['acf']['show']) || in_array('csia', $exhibitor['acf']['show'])){
                    $filters = $exhibitor['csia_activities'];
                    $filtersString = '';
    
                    $i = 1;
                    if($filters){
                        foreach($filters as $filter){
                            $filtersString .= $filter;
                            if($i < count($filters)){
                                $filtersString .= ', ';
                            }
                            $i++;
                        }
                    }
    
                    $cshaOnly = '';
                    if(!in_array('csia', $exhibitor['acf']['show'])){
                        $cshaOnly = 'csha';
                    }
    
                    if($exhibitor['title']['rendered'][0] != $currentLetter){
                        $currentLetter = $exhibitor['title']['rendered'][0];
                        $useLetter = $exhibitor['title']['rendered'][0];
                    }else{
                        $useLetter = '';
                    }
    
                    $content = apply_filters('the_content', $exhibitor['content']['rendered']);
                    $excerpt = strip_tags($content);
                    $excerpt = htmlspecialchars_decode($excerpt);
                    $excerpt = substr($excerpt, 0, 140);
                    $excerpt = $excerpt.'...';
    
                    $boothNo = '';
                    if(array_key_exists("booth_no_csia", $exhibitor['acf'])){
                        $boothNo = $exhibitor['acf']['booth_no_csia'];
                    }else{
                        $boothNo = $exhibitor['acf']['booth_no_csha'];
                    }
    
                    $mustSee = '';
                    if(array_key_exists("must_see", $exhibitor['acf'])){
                        $mustSee = $exhibitor['acf']['must_see'];
                    }else{
                        $mustSee = false;
                    }
                    $foundingPartner = '';
                    if(array_key_exists("founding_partner_us", $exhibitor['acf'])){
                        $foundingPartner = $exhibitor['acf']['founding_partner_us'];
                    }else{
                        $foundingPartner = false;
                    }
                    $exhibitorEmail = '';
                    if(array_key_exists("exhibitor_email", $exhibitor['acf'])){
                        $exhibitorEmail = $exhibitor['acf']['exhibitor_email'];
                    }
                    $urlFacebook = '';
                    if(array_key_exists("facebook_url", $exhibitor['acf'])){
                        $urlFacebook = $exhibitor['acf']['facebook_url'];
                    }
                    $urlInstagram = '';
                    if(array_key_exists("instagram_url", $exhibitor['acf'])){
                        $urlInstagram = $exhibitor['acf']['instagram_url'];
                    }
                    $urlLinkedIn = '';
                    if(array_key_exists("linkedin_url", $exhibitor['acf'])){
                        $urlLinkedIn = $exhibitor['acf']['linkedin_url'];
                    }
                    $urlTwitter = '';
                    if(array_key_exists("twitter_url", $exhibitor['acf'])){
                        $urlTwitter = $exhibitor['acf']['twitter_url'];
                    }
                    $gallery = '';
                    if(array_key_exists("gallery", $exhibitor['acf'])){
                        foreach($exhibitor['acf']['gallery'] as $image){
                            $gallery .= $image['url'].',';
                        }
                    }
    
                    $exhibitorHTML .= \App\template('partials.components.exhibitor', [
                        'id' => $exI,
                        'currentLetter' => $useLetter,
                        'csha' => $cshaOnly,
                        'title' => html_entity_decode($exhibitor['title']['rendered']),
                        'content' => $content,
                        'excerpt' => $excerpt,
                        'logo' => $exhibitor['acf']['logo'],
                        'background_image' => $exhibitor['acf']['background_image'],
                        'url' => $exhibitor['acf']['url'],
                        'booth_no' => $boothNo,
                        'filters' => $filtersString,
                        'foundingParter' => $foundingPartner,
                        'mustSee' => $mustSee,
                        'exhibitorEmail' => $exhibitorEmail,
                        'urlFacebook' => $urlFacebook,
                        'urlInstagram' => $urlInstagram,
                        'urlLinkedIn' => $urlLinkedIn,
                        'urlTwitter' => $urlTwitter,
                        'gallery' => $gallery
                    ]);
                    $exI++;
                }
            }
        }

        return $exhibitorHTML;
    }

    public function newExhibitors(){
        $exhibitors = file_get_contents(env('EXHIBITORS'));
        $exhibitors = json_decode($exhibitors, true);
        $newExhibitors = array();

        foreach($exhibitors as $exhibitor){
            if(strtotime($exhibitor['date']) > strtotime('-14 days')){
                array_push($newExhibitors, $exhibitor);
            }
        }

        // Sort by Title
        usort($newExhibitors, function($a, $b) {
            return strtolower($a['title']['rendered']) <=> strtolower($b['title']['rendered']);
        });

        $exhibitorHTML = '';

        $exI = 1;
        if(count($newExhibitors) > 0){
            foreach($newExhibitors as $exhibitor){
                if(in_array('csia', $exhibitor['acf']['show'])){
                    $filters = $exhibitor['csia_activities'];
                    $filtersString = '';
    
                    $i = 1;
                    if($filters){
                        foreach($filters as $filter){
                            $filtersString .= $filter;
                            if($i < count($filters)){
                                $filtersString .= ', ';
                            }
                            $i++;
                        }
                    }
    
                    $content = apply_filters('the_content', $exhibitor['content']['rendered']);
                    $excerpt = strip_tags($content);
                    $excerpt = htmlspecialchars_decode($excerpt);
                    $excerpt = substr($excerpt, 0, 140);
                    $excerpt = $excerpt.'...';

                    $boothNo = '';
                    if(array_key_exists("booth_no_csia", $exhibitor['acf'])){
                        $boothNo = $exhibitor['acf']['booth_no_csia'];
                    }else{
                        $boothNo = $exhibitor['acf']['booth_no_csha'];
                    }
    
                    $mustSee = '';
                    if(array_key_exists("must_see", $exhibitor['acf'])){
                        $mustSee = $exhibitor['acf']['must_see'];
                    }else{
                        $mustSee = false;
                    }
                    $foundingPartner = '';
                    if(array_key_exists("founding_partner_us", $exhibitor['acf'])){
                        $foundingPartner = $exhibitor['acf']['founding_partner_us'];
                    }else{
                        $foundingPartner = false;
                    }
                    $exhibitorEmail = '';
                    if(array_key_exists("exhibitor_email", $exhibitor['acf'])){
                        $exhibitorEmail = $exhibitor['acf']['exhibitor_email'];
                    }
                    $urlFacebook = '';
                    if(array_key_exists("facebook_url", $exhibitor['acf'])){
                        $urlFacebook = $exhibitor['acf']['facebook_url'];
                    }
                    $urlInstagram = '';
                    if(array_key_exists("instagram_url", $exhibitor['acf'])){
                        $urlInstagram = $exhibitor['acf']['instagram_url'];
                    }
                    $urlLinkedIn = '';
                    if(array_key_exists("linkedin_url", $exhibitor['acf'])){
                        $urlLinkedIn = $exhibitor['acf']['linkedin_url'];
                    }
                    $urlTwitter = '';
                    if(array_key_exists("twitter_url", $exhibitor['acf'])){
                        $urlTwitter = $exhibitor['acf']['twitter_url'];
                    }
                    $gallery = '';
                    if(array_key_exists("gallery", $exhibitor['acf'])){
                        foreach($exhibitor['acf']['gallery'] as $image){
                            $gallery .= $image['url'].',';
                        }
                    }
    
                    $exhibitorHTML .= \App\template('partials.components.exhibitor', [
                        'id' => $exI,
                        'title' => $exhibitor['title']['rendered'],
                        'content' => $content,
                        'excerpt' => $excerpt,
                        'logo' => $exhibitor['acf']['logo'],
                        'background_image' => $exhibitor['acf']['background_image'],
                        'url' => $exhibitor['acf']['url'],
                        'booth_no' => $boothNo,
                        'filters' => $filtersString,
                        'foundingParter' => $foundingPartner,
                        'mustSee' => $mustSee,
                        'exhibitorEmail' => $exhibitorEmail,
                        'urlFacebook' => $urlFacebook,
                        'urlInstagram' => $urlInstagram,
                        'urlLinkedIn' => $urlLinkedIn,
                        'urlTwitter' => $urlTwitter,
                        'gallery' => $gallery
                    ]);
                    $exI++;
                }
            }
        }else{
            $exhibitorHTML = '<div class="col">';
            $exhibitorHTML .= '<h3 class="text-center text-primary">No exhibitors have been added in the last 2 weeks</h3>';
            $exhibitorHTML .= '</div>';
        }

        return $exhibitorHTML;
    }
}
