<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateSponsorsSupporters extends Controller
{
    public function sponsorsSupporters(){
        $sponsup_cats = get_terms( array(
            'taxonomy' => 'tier',
            'hide_empty' => false,
            'orderby' => 'description'
        ));

        $sponsupHTML = '';
        $i = 1;
        foreach($sponsup_cats as $sponsup){
            $sponsupHTML .= \App\template('partials.components.sponsor-supporters.sponsor-supporters-title', [
                'tier' => $sponsup->name,
            ]);

            $sponsups_posts = get_posts([
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'tier',
                        'field' => 'slug', 
                        'terms' => $sponsup->slug
                    )),
                'post_type' => 'sponsors_supporters',
            ]);
            
            $sponsupHTML .= '<div class="row justify-content-center">';
            foreach($sponsups_posts as $post){
                $sponsupHTML .= \App\template('partials.components.sponsor-supporters.sponsor-supporter', [
                    'id' => $i,
                    'name' => $post->post_title,
                    'logo' => get_field('logo', $post->ID),
                    'booth_no' => get_field('booth_no', $post->ID),
                    'url' => get_field('url', $post->ID),
                    'body' => $post->post_content,
                ]);
                $i++;
            }
            $sponsupHTML .= '</div>';
        }

        return $sponsupHTML;
    }
}
