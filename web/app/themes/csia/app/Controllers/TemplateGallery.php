<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateGallery extends Controller
{
    public function gallery(){
        $i = 0;
        $tabI = 1;

        $galleryHTML = '';
        $galleryModalHTML = '';

        foreach(get_field('gallery_images') as $image){
            if($i % get_field('images_per_page') == 0 && $i != 0){
                $tabI++;
                $galleryHTML .= '</div></div>';
                $galleryHTML .= '<div role="tabpanel" class="tab-pane fade" id="gallery-'.$tabI.'">';
                $galleryHTML .= '<div class="row">';
            }
            
            $galleryHTML .= \App\template('partials.components.gallery-image', [
                'img' => $image['url'],
                'pos' => $i,
            ]);

            $galleryModalHTML .= '<img class="img-fluid mx-auto d-block" src="'.$image['url'].'" />';

            $i++;
        }

        return array(
            'gallery' => $galleryHTML,
            'modal' => $galleryModalHTML
        );
    }

    public function galleryPagination(){
        $i = 0;
        $tabI = 1;

        $paginationHTML = '';

        foreach(get_field('gallery_images') as $image){
            if($i % get_field('images_per_page') == 0 && $i != 0){
                $tabI++;

                $paginationHTML .= '<li class="nav-item">';
                $paginationHTML .= '<a href="#gallery-'.$tabI.'" aria-controls="gallery-'.$tabI.'" role="tab" data-toggle="tab" class="nav-link">'.$tabI.'</a>';
                $paginationHTML .= '</li>';
            }
            $i++;
        }

        return $paginationHTML;
    }
}
