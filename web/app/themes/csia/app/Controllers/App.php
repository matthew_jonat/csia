<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function sections(){

        if(get_field('sections')){
            // Initialize the return variable
            $return = '';

            $sections = get_field('sections');
            
            foreach($sections as $section){
                switch($section['section_select']){
                    case '1_col':
                        $return .= \App\template('partials.sections.1-col', [
                            'one_col' => $section['column_1_text'],
                        ]);
                        break;
                    case '2_col':
                        $lCol = '';
                        $rCol = '';

                        switch($section['two_col_layout']){
                            case '5050':
                                $lCol = '-sm-6';
                                $rCol = '-sm-6';
                                break;
                            case '7030':
                                $lCol = '-sm-7';
                                $rCol = '-sm-5';
                                break;
                            case '3070':
                                $lCol = '-sm-5';
                                $rCol = '-sm-7';
                                break;
                        }

                        $return .= \App\template('partials.sections.2-col', [
                            'lCol' => $lCol,
                            'rCol' => $rCol,
                            'one_col' => $section['column_1_text'],
                            'two_col' => $section['column_2_text'],
                        ]);

                        break;
                    case '3_col':
                        $return .= \App\template('partials.sections.3-col', [
                            'one_col' => $section['column_1_text'],
                            'two_col' => $section['column_2_text'],
                            'three_col' => $section['column_3_text'],
                        ]);
                        break;
                    case 'half_image':
                        $ctas = '';

                        if($section['ctas']){
                            foreach($section['ctas'] as $cta){
                                if($section['switch_sides']){
                                    $ctas .= \App\makeCTA($cta, 'full-width primary', true);
                                }else{
                                    $ctas .= \App\makeCTA($cta, 'full-width', true);
                                }
                            }
                        }
                        
                        if($section['title']){
                            $return .= \App\template('partials.sections.half-image', [
                                'switch' => $section['switch_sides'],
                                'title' => $section['title'],
                                'text' => $section['text'],
                                'ctas' => $ctas,
                                'image' => $section['image'],
                                'show_slides' => $section['slideshow'],
                                'slides' => $section['slides'],
                            ]);
                        }
                        
                        break;
                    case 'stat_counter':
                        $statsHTML = '';

                        foreach($section['stats'] as $stat){
                            $statsHTML .= \App\template('partials.sections.stat-counter', [
                                'label' => $stat['label'],
                                'amount' => $stat['amount'],
                            ]);
                        }

                        $return .= \App\template('partials.sections.stat-counter-container', [
                            'stats' => $statsHTML,
                        ]);

                        break;
                    case '4_tile':
                        $tileHTML = '';

                        foreach($section['4_tile'] as $tile){
                            $tileHTML .= \App\template('partials.sections.four-tile', [
                                'title' => $tile['title'],
                                'text' => $tile['text'],
                                'page' => $tile['page'],
                                'background' => $tile['background'],
                            ]);
                        }

                        $return .= \App\template('partials.sections.four-tile-container', [
                            'fourTile' => $tileHTML,
                        ]);
                        
                        break;
                    case '3_tile':
                        $tileHTML = '';
                        $i = 0;

                        foreach($section['3_tile'] as $tile){
                            $tileHTML .= \App\template('partials.sections.three-tile', [
                                'i' => $i,
                                'title' => $tile['title'],
                                'text' => $tile['text'],
                                'page' => $tile['page'],
                                'background' => $tile['background'],
                            ]);
                            $i++;
                        }

                        $return .= \App\template('partials.sections.three-tile-container', [
                            'threeTile' => $tileHTML,
                        ]);
                        
                        break;
                    case 'logos':
                        $logosHTML = '';
                        $logoType = $section['logos_select'];

                        $logos = get_posts([
                            'post_type' => $logoType,
                            'posts_per_page'=> -1,
                            'orderby' => 'title',
                            'order' => 'ASC'
                        ]);

                        foreach($logos as $logo){
                            $logosHTML .= \App\template('partials.sections.logos', [
                                'img' => get_field('logo', $logo->ID),
                                'url' => get_field('url', $logo->ID),
                                'alt' => $logo->post_title
                            ]);
                        }

                        $return .= \App\template('partials.sections.logos-container', [
                            'title' => $section['title'],
                            'logos' => $logosHTML,
                        ]);
                        break;
                    case 'full_width':
                        $ctas = '';

                        if($section['ctas']){
                            foreach($section['ctas'] as $cta){
                                $ctas .= \App\makeCTA($cta, 'full-width', true);
                            }
                        }

                        if($section['title']){
                            $return .= \App\template('partials.sections.full-width', [
                                'title' => $section['title'],
                                'text' => $section['text'],
                                'ctas' => $ctas,
                                'image' => $section['image'],
                            ]);
                        }
                        
                        break;
                    case 'instagram':
                        $return .= \App\template('partials.sections.instagram');
                        break;
                    case 'testimonials':
                        $testimonialsHTML = '';

                        foreach($section['testimonials'] as $testimonial){
                            $testimonialsHTML .= \App\template('partials.sections.testimonials', [
                                'testimonial' => $testimonial['testimonial'],
                                'author' => $testimonial['author'],
                            ]);
                        }

                        $return .= \App\template('partials.sections.testimonials-container', [
                            'testimonials' => $testimonialsHTML,
                        ]);

                        break;
                    case 'line_break':
                        $return .= '<hr>';
                        break;
                }
            }

            return $return;
        }else{
            return null;
        }
    }

    // Footer fields
    public function footerFields() {
        return array (
            'venue' => get_field('venue', 'option'),
            'opening_times' => get_field('opening_times', 'option'),
            'quick_links' => get_field('quick_links', 'option'),
            'twitter_url' => get_field('twitter_url', 'option'),
            'facebook_url' => get_field('facebook_url', 'option'),
            'instagram_url' => get_field('instagram_url', 'option'),
            'linkedin_url' => get_field('linkedin_url', 'option'),
        );
    }

    public function headerFields() {
        return array (
            'left_cta_internal' => get_field('left_cta_internal', 'option'),
            'right_cta_internal' => get_field('right_cta_internal', 'option'),
            'left_cta_external' => get_field('left_cta_external', 'option'),
            'right_cta_external' => get_field('right_cta_external', 'option'),
            'left_cta_text' => get_field('left_cta_text', 'option'),
            'right_cta_text' => get_field('right_cta_text', 'option'),
        );
    }

    public function centerHeader() {
        if(get_field('center_align_header')){
            return array(
                'text-center' => 'text-center',
                'center-title' => 'center-title',
            );
        }else{
            return array(
                'text-center' => null,
                'center-title' => null,
            );
        }
    }

    public function centerHeaderBlogPostsPage() {
        if(get_field('center_align_header', get_option('page_for_posts'))){
            return array(
                'text-center' => 'text-center',
                'center-title' => 'center-title',
            );
        }else{
            return array(
                'text-center' => null,
                'center-title' => null,
            );
        }
    }

    public static function excerpt($limit) {
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }	
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return $excerpt;
    }
}
