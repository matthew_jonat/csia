<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Single extends Controller
{
    public function relatedPosts(){
        // First we get tags of current post
        $tagsArr = get_the_tags(get_the_ID());

        $postsHTML = '';

        // Then check if we actually have any tags to work with
        if($tagsArr){
            // Then put all those tags into an array
            $tagIDsArr = [];
            foreach($tagsArr as $tag){
                array_push($tagIDsArr, $tag->term_id);
            }

            // Then get posts with those tags
            $relatedPosts = get_posts([
                'post_type' => 'post',
                'numberposts' => 10,
                'orderby' => 'date',
                'order' => 'DESC',
                'tag__in' => $tagIDsArr
            ]);

            // Loop through posts and get the data into html
            foreach($relatedPosts as $post){
                $postsHTML .= \App\template('partials.components.latest-post', [
                    'title' => $post->post_title,
                    'thumbnail' => get_the_post_thumbnail_url($post->ID),
                    'url' => get_the_permalink($post->ID),
                ]);
            }

        // If we dont, just show latest posts
        }else{
            $latestPosts = get_posts([
                'post_type' => 'post',
                'numberposts' => 10,
                'orderby' => 'date',
                'order' => 'DESC'
            ]);
    
            foreach($latestPosts as $post){
                $postsHTML .= \App\template('partials.components.latest-post', [
                    'title' => $post->post_title,
                    'thumbnail' => get_the_post_thumbnail_url($post->ID),
                    'url' => get_the_permalink($post->ID),
                ]);
            }
        }

        return $postsHTML;
    }
}
