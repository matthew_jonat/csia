<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateConference extends Controller
{
    public function day1(){
        $return = "";
        $sid = 1;

        $day1 = get_field('conf_day_1');

        foreach($day1 as $session){
            $speakerHTML = "";
            $mid = "";

            if($session['moderator']){
                $mid = $sid;
                $sid++;
            }

            if($session['speakers']){
                foreach($session['speakers'] as $speaker){
                    $speakerHTML .= \App\template('partials.components.conf.speaker', [
                        'id' => $sid,
                        'name' => $speaker->post_title,
                        'job_title_company' => get_field('job_title_company', $speaker->ID),
                        'headshot' => get_field('headshot', $speaker->ID),
                        'logo' => get_field('logo', $speaker->ID),
                        'body' => $speaker->post_content
                    ]);
                    $sid++;
                }
            }

            $return .= \App\template('partials.components.conf.session', [
                'title' => $session['title'],
                'time_start' => $session['start_time'],
                'time_end' => $session['end_time'],
                'break' => $session['break'],
                'body' => $session['session_info'],
                'mid' => $mid,
                'moderator' => $session['moderator'],
                'speakers' => $speakerHTML
            ]);
        }

        return array(
            'totalSpeakers' => $sid,
            'sessions' => $return
        );
    }

    public function day2(){
        $return = "";
        $sid = 100;

        $day2 = get_field('conf_day_2');

        foreach($day2 as $session){
            $speakerHTML = "";
            $mid = "";

            if($session['moderator']){
                $mid = $sid;
                $sid++;
            }

            if($session['speakers']){
                foreach($session['speakers'] as $speaker){
                    $speakerHTML .= \App\template('partials.components.conf.speaker', [
                        'id' => $sid,
                        'name' => $speaker->post_title,
                        'job_title_company' => get_field('job_title_company', $speaker->ID),
                        'headshot' => get_field('headshot', $speaker->ID),
                        'logo' => get_field('logo', $speaker->ID),
                        'body' => $speaker->post_content
                    ]);
                    $sid++;
                }
            }

            $return .= \App\template('partials.components.conf.session', [
                'title' => $session['title'],
                'time_start' => $session['start_time'],
                'time_end' => $session['end_time'],
                'break' => $session['break'],
                'body' => $session['session_info'],
                'mid' => $mid,
                'moderator' => $session['moderator'],
                'speakers' => $speakerHTML
            ]);
        }

        return array(
            'totalSpeakers' => $sid,
            'sessions' => $return
        );
    }
}
