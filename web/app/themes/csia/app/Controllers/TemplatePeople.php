<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplatePeople extends Controller
{
    public function people(){
        $return = '';

        $people = get_posts([
            'post_type' => get_field('people'),
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => 'ASC'
        ]);

        if($people){
            $i = 1;
            foreach($people as $person){
                $return .= \App\template('partials.components.person', [
                    'id' => $i,
                    'name' => $person->post_title,
                    'job_title_company' => get_field('job_title_company', $person->ID),
                    'headshot' => get_field('headshot', $person->ID),
                    'logo' => get_field('logo', $person->ID),
                    'body' => $person->post_content,
                ]);
                $i++;
            }
        }else{
            $return .= '<p class="text-warning">';
            $return .= 'There are no people in this post type yet';
            $return .= '</p>';
        }
        

        return $return;
    }
}