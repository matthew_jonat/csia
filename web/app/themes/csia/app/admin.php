<?php

namespace App;

/**
 * Theme customizer
 */
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    // Add postMessage support
    $wp_customize->get_setting('blogname')->transport = 'postMessage';
    $wp_customize->selective_refresh->add_partial('blogname', [
        'selector' => '.brand',
        'render_callback' => function () {
            bloginfo('name');
        }
    ]);
});

/**
 * Customizer JS
 */
add_action('customize_preview_init', function () {
    wp_enqueue_script('sage/customizer.js', asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
});


// Settings page in admin
// First we check to see if acf_add_options_page is a function.
// If it is not, then we probably do not have ACF Pro installed
if( function_exists('acf_add_options_page') ) {
    // Let's add our Options Page
    acf_add_options_page(array(
        'page_title'    => 'Theme Options',
        'menu_title'    => 'Theme Options',
        'menu_slug'     => 'theme-options',
        'capability'    => 'edit_posts'
    ));
    
    // If we want to add multiple sections to our Options Page
    // we can do so with an Options Sub Page.
    acf_add_options_sub_page(array(
        'page_title'    => "Header Settings",
        'parent_slug'   => 'theme-options',  // 'menu_slug' on the parent options page
        'menu_title'    => "Header Settings",
        'menu_slug'     => 'header-settings',
    ));
    
    acf_add_options_sub_page(array(
        'page_title'    => 'Footer Settings',
        'parent_slug'   => 'theme-options',
        'menu_title'    => 'Footer Settings',
        'menu_slug'     => 'footer-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Register Modal',
        'parent_slug'   => 'theme-options',
        'menu_title'    => 'Register Modal',
        'menu_slug'     => 'register-modal-settings',
    ));
}