<form role="search" method="get" action="{{ esc_url( home_url( '/' ) ) }}">
    <div class="form-row">
        <div class="col-8 col-sm-10">
            <input type="search" class="form-control" placeholder="{!! esc_attr_x( 'Search &hellip;', 'placeholder' ) !!}" value="{{ get_search_query() }}" name="s">
        </div>
        <div class="col-4 col-sm-2">
            <button type="submit" class="btn btn-primary mb-2">{{ esc_attr_x( 'Search', 'submit button' ) }}</button>
        </div>
    </div>
</form>