<header class="banner">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-6 col-md-12 col-lg-6 center-logo-sm">
                <div class="logo-container">
                    <a href="/" class="logo init"><img src="@asset('images/logo-csia.svg')" class="img-fluid" alt="Cruise Ship Interiors Europe"></a>
                    <a href="/" class="logo scroll-logo"><img src="@asset('images/logo-csia-inline-inverted.svg')" class="img-fluid" alt="Cruise Ship Interiors Europe"></a>
                    <div class="co-located-container">
                        <p class="co-located-text">CO-LOCATED WITH</p>
                        <a href="https://cruiseshiphospitality-expo.com/" target="_blank">
                            <img src="@asset('images/logo-csha.svg')" alt="Cruise Ship Hopsitality America" class="d-block img-fluid" />
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-2 offset-4 offset-sm-0 order-sm-12 d-block d-lg-none alpha">
                <button class="navbar-toggler pull-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-navicon"></i>
                </button>
            </div>
            <div class="col-12 order-sm-1 col-md-10 col-lg-6">
                <p class="date-loc">16 &#8211; 17 JUNE 2020 | MIAMI BEACH CONVENTION CENTER</p>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light">
        @if (has_nav_menu('primary_navigation'))
            {!! 
                wp_nav_menu( array(
                    'theme_location'  => 'primary_navigation',
                    'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
                    'container'       => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id'    => 'navbarSupportedContent',
                    'menu_class'      => 'navbar-nav mr-auto',
                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                    'walker'          => new WP_Bootstrap_Navwalker(),
                ) );
            !!}
        @endif
    </nav>
    <div class="container-fluid header-ctas nopadding">
        <div class="row">
            <div class="col bg-grey">
                <p class="text-center left">
                    @if($header_fields['left_cta_internal'])
                        <a href="{{ $header_fields['left_cta_internal'] }}" class="text-primary">{{ $header_fields['left_cta_text'] }}</a>
                    @else
                        <a href="{{ $header_fields['left_cta_external'] }}" class="text-primary" target="_blank">{{ $header_fields['left_cta_text'] }}</a>
                    @endif
                </p>
            </div>
            <div class="col bg-primary">
                <p class="text-center right">
                    @if($header_fields['right_cta_internal'])
                        <a href="{{ $header_fields['right_cta_internal'] }}" class="text-white">{{ $header_fields['right_cta_text'] }}</a>
                    @else
                        <a href="{{ $header_fields['right_cta_external'] }}" class="text-white" target="_blank">{{ $header_fields['right_cta_text'] }}</a>
                    @endif
                </p>
            </div>
        </div>
    </div>
</header>
