<section class="container page-header blog-post-header">
    <div class="row">
        <div class="col-10 offset-1 col-sm-8 offset-sm-2 text-center">
            <div class="title center-title">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary">
                    <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
                </svg>
                <h1 class="text-primary text-center">{!! App::title() !!}</h1>
            </div>
        </div>
    </div>
    <div class="row pusher pusher-more-sm blog-post-meta">
        <div class="col-10 offset-1 col-sm-8 offset-sm-2">
            <div class="row">
                <div class="col-8">
                    {!! get_wp_user_avatar() !!}
                    <p>
                        Written by {{ get_the_author_meta('first_name') }} {{ get_the_author_meta('last_name') }}<br>
                        {{ get_the_author_meta('description') }}
                    </p>
                </div>
                <div class="col-4 text-primary text-center"> 
                    {{ get_the_date('l jS F Y') }}
                </div>
            </div>
        </div>
        
    </div>
</section>