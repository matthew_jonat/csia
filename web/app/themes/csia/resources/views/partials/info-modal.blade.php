<div class="modal fade info-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-modal-id="">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary exit d-sm-none" data-dismiss="modal">
                <polygon points="109.89,0.05 28.52,81.42 -0.24,110.18 28.16,138.58 109.53,219.95 172.84,219.95 63.07,110.18 173.2,0.05 "/>
            </svg>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary prev">
                <polygon points="109.89,0.05 28.52,81.42 -0.24,110.18 28.16,138.58 109.53,219.95 172.84,219.95 63.07,110.18 173.2,0.05 "/>
            </svg>
            <div class="row align-items-center">
                <div class="col-12 col-sm-4">
                    <img src="" alt="" class="left-img img-fluid d-block mx-auto">
                </div>
                <div class="col-12 col-sm-8">
                    <p class="modal-info"></p>
                    <img src="" alt="" class="right-img img-fluid">
                    <div class="body"></div>
                    <p class="hidden-cta">
                        <a href="" class="cta primary" target="_blank">VISIT WEBSITE</a>
                    </p>
                </div>
            </div>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary next">
                <polygon points="62.84,219.95 144.21,138.58 172.97,109.82 144.57,81.42 63.2,0.05 -0.1,0.05 109.66,109.82 -0.47,219.95 "/>
            </svg>
        </div>
    </div>
</div>