<div class="col-12 col-sm-3">
    <p class="text-center">
        <span class="stat amount" data-stat-amount="{{ $amount }}"></span>
    </p>
    <p class="text-center">
        <span class="stat label">{{ $label }}</span>
    </p>
</div>