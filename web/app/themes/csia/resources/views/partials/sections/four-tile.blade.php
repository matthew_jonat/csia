<div class="col-12 col-sm-6">
    <div class="tile">
        <div class="tile-img cover-bg" style="background-image: url({{ $background }})">
            <a href="{{ $page }}" class="cover-link"></a>
            <div class="tile-content bg-blend d-none d-sm-block">
                <div class="content text-white">
                    {!! $text !!}
                </div>
            </div>
        </div>
        <div class="tile-title">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" style="enable-background:new 0 0 200 200;" xml:space="preserve" class="arrow primary d-none d-sm-block">
                <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
            </svg>
            <h3><a href="{{ $page }}">{{ $title }}</a></h3>
        </div>
        <div class="tile-content text-primary d-block d-sm-none">
            {!! $text !!}
        </div>
    </div>
</div>