<section class="container-fluid">
    <div class="row">
        <div class="col">
            {!! $one_col !!}
        </div>
        <div class="col">
            {!! $two_col !!}
        </div>
        <div class="col">
            {!! $three_col !!}
        </div>
    </div>
</section>