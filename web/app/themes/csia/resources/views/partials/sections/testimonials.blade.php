<div class="testimonial text-center text-primary">
    {!! $testimonial !!}
    <p class="author">{{ $author }}</p>
</div>