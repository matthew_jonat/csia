<div class="col-12 col-sm-4">
    <div class="tile-3" data-i="{{ $i }}">
        <a href="{{ $page }}" class="cover-link"></a>
        <div class="tile-img cover-bg" style="background-image: url({{ $background }})">
            <div class="tile-content bg-blend">
                <div class="content">
                    {!! $text !!}
                </div>
            </div>
            <div class="tile-title bg-blend">
                <div class="content">
                    <h3>{{ $title }}</h3>
                </div>
            </div>
        </div>
    </div>
</div>