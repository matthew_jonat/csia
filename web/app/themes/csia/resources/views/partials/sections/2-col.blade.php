<section class="container-fluid">
    <div class="row">
        <div class="col-12 col{{ $lCol }}">
            {!! $one_col !!}
        </div>
        <div class="col-12 col{{ $rCol }}">
            {!! $two_col !!}
        </div>
    </div>
</section>