<section class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="bg-grey">
                <div class="row">
                    <div class="col-12">
                        <div class="testimonials">
                            {!! $testimonials !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="testimonial-dots"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>