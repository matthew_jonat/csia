<section class="container-fluid bg-grey instagram-container">
    <div class="row">
        <div class="col">
            <h4 class="text-center text-primary">Instagram feed</h4>
            <p class="text-center text-primary">Latest trends, industry news and much more...</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div id="instafeed"></div>
            <p class="d-block d-md-none pusher text-center"><a href="https://www.instagram.com/csie.expo/" class="cta primary">Follow Us</a></p>
        </div>
    </div>
</section>