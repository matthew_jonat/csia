<section class="container-fluid full-width-section cover-bg" style="background-image: url({{ $image }})">
    <div class="row">
        <div class="col-6">
            <div class="full-width-title">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow white d-none d-sm-block">
                    <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
                </svg>
                <h2>{{ $title }}</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-10 col-sm-6">
            {!! $text !!}
            {!! $ctas !!}
        </div>
    </div>
</section>