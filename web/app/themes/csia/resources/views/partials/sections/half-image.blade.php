@if($switch)
    <section class="container-fluid half-img switch nopadding">
        <div class="row">
            <div class="col-12 col-sm-6 order-md-12">
                <div class="half-img-text">
                    <div class="half-img-title">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary">
                            <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
                        </svg>
                        <h2 class="text-primary">{{ $title }}</h2>
                        {!! $text !!}
                        {!! $ctas !!}
                    </div>
                </div>
            </div>
            @if($show_slides)
                <div class="col-12 col-sm-6 half-img-slides-container">
                    <div class="half-img-slides">
                        @foreach($slides as $slide)
                            <div class="half-img-slide cover-bg" style="background-image: url({{ $slide }})"></div>
                        @endforeach
                    </div>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary transparent">
                        <polygon points="44.87,200 159.95,200 200.62,200 200.62,159.84 200.62,44.76 155.86,0 155.86,155.24 0.11,155.24 "/>
                    </svg>
                </div>
            @else
                <div class="col-12 col-sm-6 order-md-1 cover-bg half-img-img" style="background-image: url({{ $image }})">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary transparent d-block d-sm-none">
                        <polygon points="44.87,200 159.95,200 200.62,200 200.62,159.84 200.62,44.76 155.86,0 155.86,155.24 0.11,155.24 "/>
                    </svg>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary transparent d-none d-sm-block">
                        <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
                    </svg>
                </div>
            @endif
        </div>
    </section>
@else
    <section class="container-fluid half-img nopadding">
        <div class="row bg-primary">
            <div class="col-12 col-sm-6">
                <div class="half-img-text">
                    <div class="half-img-title text-white">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow white">
                            <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
                        </svg>
                        <h2>{{ $title }}</h2>
                        {!! $text !!}
                        {!! $ctas !!}
                    </div>
                </div>
            </div>
            @if($show_slides)
                <div class="col-12 col-sm-6 half-img-slides-container">
                    <div class="half-img-slides">
                        @foreach($slides as $slide)
                            <div class="half-img-slide cover-bg" style="background-image: url({{ $slide }})"></div>
                        @endforeach
                    </div>
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary transparent">
                        <polygon points="44.87,200 159.95,200 200.62,200 200.62,159.84 200.62,44.76 155.86,0 155.86,155.24 0.11,155.24 "/>
                    </svg>
                </div>
            @else
                <div class="col-12 col-sm-6 cover-bg half-img-img" style="background-image: url({{ $image }})">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve" class="arrow primary transparent">
                        <polygon points="44.87,200 159.95,200 200.62,200 200.62,159.84 200.62,44.76 155.86,0 155.86,155.24 0.11,155.24 "/>
                    </svg>
                </div>
            @endif
        </div>
    </section>
@endif