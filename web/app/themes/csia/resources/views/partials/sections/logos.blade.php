<div class="logo">
    @if($url)
        <a href="{{ $url }}" target="_blank">
    @endif
    <img src="{{ $img }}" alt="{{ $alt }}" class="img-fluid mx-auto">
    @if($url)
        </a>
    @endif
</div>