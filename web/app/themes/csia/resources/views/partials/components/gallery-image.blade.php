<div class="col-6 col-sm-4">
    <div class="gallery-image" data-img-url="{{ $img }}" data-pos="{{ $pos }}">
        <div class="image" style="background-image:url('{{ $img }}');"></div>
        <div class="loader"></div>
    </div>
</div>