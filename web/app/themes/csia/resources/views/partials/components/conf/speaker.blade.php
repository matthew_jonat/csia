<div class="row speaker align-items-center" data-id="{{ $id }}">
    <div class="col-3 col-lg-2 omega">
        <img src="{{ $headshot }}" alt="" class="img-fluid headshot">
    </div>
    <div class="col-3 col-lg-2 omega">
        <img src="{{ $logo }}" alt="" class="img-fluid logo">
    </div>
    <div class="col-6 col-lg-8">
        <p class="person-info">
            <span class="name">{{ $name }}</span><br>
            {{ $job_title_company }}
        </p>
        <div class="body d-none">
            {!! $body !!}
        </div>
    </div>
</div>