<div class="latest-post">
    <div class="blog-img cover-bg" style="background-image: url({{ $thumbnail }})">
        <a href="{{ $url }}" class="cover-link"></a>
    </div>
    <h4><a href="{{ $url }}">{{ $title }}</a></h4>
</div>