<div class="col-6 col-sm-4 exhibitor-container">
    <div id="{{ $currentLetter }}" 
        class="exhibitor cover-bg pusher-bottom pusher-bottom-sm {{ $csha }}" 
        style="background-image:url({{ $background_image }})" 
        data-logo="{{ $logo }}" 
        data-url="{{ $url }}" 
        data-filters="{{ $filters }}" 
        data-id="{{ $id }}"
        data-founding-partner="{{ $foundingParter }}"
        data-must-see="{{ $mustSee }}"
        data-email="{{ $exhibitorEmail }}"
        data-url-facebook="{{ $urlFacebook }}"
        data-url-instagram="{{ $urlInstagram }}"
        data-url-linkedin="{{ $urlLinkedIn }}"
        data-url-twitter="{{ $urlTwitter }}"
        data-gallery="{{ $gallery }}">
        @if($foundingParter)
            <div class="featured-tri founding-partner">
                <p>
                    <strong>FOUNDING</strong><br class="d-none d-md-block">
                    PARTNER
                </p>
            </div>
        @endif
        @if($mustSee)
            <div class="featured-tri must-see">
                <div class="must-see-inside">
                    <p>
                        <strong>
                            MUST<br class="d-none d-md-block">
                            SEE!
                        </strong>
                    </p>
                </div>
            </div>
        @endif
        <div class="bg-blend">
            <div class="content text-white">
                <div class="exhibitor-info">
                    <h3>{{ $title }}</h3>
                    @if($booth_no)
                        <p class="booth">BOOTH {{ $booth_no }}</p>
                    @endif
                </div>
                <div class="body d-none d-sm-block">
                    <p>{{ $excerpt }}</p>
                </div>
                <div class="body-full d-none">
                    {!! $content !!}
                </div>
            </div>
        </div>
    </div>
</div>