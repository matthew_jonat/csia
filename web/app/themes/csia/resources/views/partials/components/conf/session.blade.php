<div class="session {{ $break ? 'break' : '' }}">
    <div class="session-head">
        @if(!$break)
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary show-session">
                <polygon points="62.84,219.95 144.21,138.58 172.97,109.82 144.57,81.42 63.2,0.05 -0.1,0.05 109.66,109.82 -0.47,219.95 "/>
            </svg>
        @endif
        <div class="row">
            <div class="col-12 col-md-2 alpha omega">
                <p class="text-center times">
                    {{ $time_start }}<br class="d-none d-md-block">
                    <span class="timeto">
                        &#8211;<br class="d-none d-md-block">
                        {{ $time_end }}
                    </span>
                </p>
            </div>
            <div class="col-12 col-md-10">
                <h3 class="{{ $break ? '' : 'show-session' }}">{{ $title }}</h3>
            </div>
        </div>
    </div>
    @if(!$break)
        <div class="session-body">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    {!! $body !!}
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    @if($moderator)
                        <div class="moderator">
                            <div class="row">
                                <div class="col">
                                    <p><strong>Moderator: </strong></p>
                                </div>
                            </div>
                            <div class="row speaker align-items-center" data-id="{{ $mid }}">
                                <div class="col-3 col-lg-2 omega">
                                    <img src="{{ get_field('headshot', $moderator->ID) }}" alt="" class="img-fluid headshot">
                                </div>
                                <div class="col-3 col-lg-2 omega">
                                    <img src="{{ get_field('logo', $moderator->ID) }}" alt="" class="img-fluid logo">
                                </div>
                                <div class="col-6 col-lg-8">
                                    <p class="person-info">
                                        <span class="name">{{ $moderator->post_title }}</span><br>
                                        {{ get_field('job_title_company', $moderator->ID) }}
                                    </p>
                                    <div class="body d-none">
                                        {!! $moderator->post_content !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-10 offset-md-2">
                    @if($speakers)
                        <div class="speakers">
                            <div class="row">
                                <div class="col">
                                    <p><strong>Speakers: </strong></p>
                                </div>
                            </div>
                            {!! $speakers !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    @endif
</div>