<div class="col-12 col-sm-4 col-lg-3 alpha omega">
    <div class="sponsup" data-url="{{ $url }}" data-id="{{ $id }}">
        <div class="logo">
            <img src="{{ $logo }}" alt="{{ $name }}" class="img-fluid d-block mx-auto">
        </div>
        <p class="sponsup-info d-none">
            <span class="name">{{ $name }}</span><br>
            <span class="boothno-container">Booth: <span class="boothno">{{ $booth_no }}</span></span>
        </p>
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary d-block d-sm-none">
            <polygon points="62.84,219.95 144.21,138.58 172.97,109.82 144.57,81.42 63.2,0.05 -0.1,0.05 109.66,109.82 -0.47,219.95 "/>
        </svg>
        <div class="body d-none">
            {!! $body !!}
        </div>
    </div>
</div>