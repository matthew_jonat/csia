<footer>
    <section class="container-fluid bg-primary text-white">
        <div class="row">
            <div class="col-12 col-sm-3">
                <h2>Venue</h2>
                {!! $footer_fields['venue'] !!}
            </div>
            <div class="col-12 col-sm-3">
                <h2>Opening Times</h2>
                {!! $footer_fields['opening_times'] !!}
            </div>
            <div class="col-12 col-sm-3">
                <h2>Quick Links</h2>
                <ul class="quick-links">
                    @foreach($footer_fields['quick_links'] as $link)
                        <li><a href="{{ get_the_permalink($link->ID) }}" class="text-white">{{ $link->post_title }}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12 col-sm-3">
                <h2>Follow Us</h2>
                <p>
                    @if($footer_fields['twitter_url'])
                        <a href="{{ $footer_fields['twitter_url'] }}" class="text-white social" target="_blank"><i class="fa fa-twitter"></i></a>
                    @endif
                    @if($footer_fields['facebook_url'])
                        <a href="{{ $footer_fields['facebook_url'] }}" class="text-white social" target="_blank"><i class="fa fa-facebook"></i></a>
                    @endif
                    @if($footer_fields['instagram_url'])
                        <a href="{{ $footer_fields['instagram_url'] }}" class="text-white social" target="_blank"><i class="fa fa-instagram"></i></a>
                    @endif
                    @if($footer_fields['linkedin_url'])
                        <a href="{{ $footer_fields['linkedin_url'] }}" class="text-white social" target="_blank"><i class="fa fa-linkedin"></i></a>
                    @endif
                </p>
            </div>
        </div>
    </section>
    <section class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-4">
                @if (has_nav_menu('footer_menu'))
                    {!! wp_nav_menu(['theme_location' => 'footer_menu']) !!}
                @endif
                <p class="text-primary">
                    Copyright &copy; {{ date('Y') }} <a href="https://elite-exhibitions.com/">Elite Exhibitions</a>
                </p>
                <p class="text-primary">
                    This site was built and is maintained by <a href="https://gooi.ltd" class="text-primary">Gooi Ltd</a>
                </p>
            </div>
            <div class="col-sm-4 alpha-sm omega-sm other-shows">
                <div class="row pusher">
                    <div class="col-6">
                        <a href="https://cruiseshipinteriors-europe.com" target="_blank"><img src="@asset('images/logo-csie.svg')" alt="Cruise Ship Interiors Europe" class="img-fluid"></a>
                    </div>
                    <div class="col-6">
                        <a href="https://www.cruiseshiphospitality-expo.com/" target="_blank"><img src="@asset('images/logo-csha.svg')" alt="Cruise Ship Hospitality America" class="img-fluid"></a>
                    </div>
                </div>
                <div class="row pusher pusher-sm">
                    <div class="col-6">
                        <a href="https://www.cruiseshipinteriors-awards.com/" target="_blank" class="pull-left"><img src="@asset('images/logo-csi-awards.svg')" alt="Cruise Ship Interiors Awards" class="img-fluid"></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row pusher">
                    <div class="col-6">
                        <p class="text-primary text-center">Organised by:</p>
                        <a href="http://www.elite-exhibitions.com/" target="_blank"><img src="@asset('images/EE-2020-LOGO-FOR-WEB.jpg')" alt="Elite Exhibitions" class="img-fluid d-block mx-auto ee-logo"></a>
                    </div>
                    <div class="col-6">
                        <p class="text-primary text-center">Member of SISO</p>
                        <img src="@asset('images/logo-siso.jpg')" alt="SISO" class="img-fluid mx-auto">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @php dynamic_sidebar('sidebar-footer') @endphp
</footer>
<!-- Modal -->
<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="registerodal" aria-hidden="true" data-show-modal="{{ get_field('show_register_modal', 'option') }}">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="text-center text-primary">{{ get_field('modal_title', 'option') }}</h3>
                <p class="text-center">{!! get_field('modal_text', 'option') !!}</p>
                <p class="text-center"><a href="{{ get_field('modal_cta_link', 'option') }}" class="cta primary wide" target="_blank">{{ get_field('modal_cta_text', 'option') }}</a></p>
            </div>
        </div>
    </div>
</div>