<div class="col-12 col-sm-4 post-container">
    <article {{ post_class() }}>
        <div class="blog-img cover-bg" style="background-image:url({{ get_the_post_thumbnail_url() }})">
            <a href="{{ get_permalink() }}" class="cover-link"></a>
        </div>
        <p class="text-primary text-uppercase">{{ get_the_date() }}</p>
        <h2 class="entry-title"><a href="{{ get_permalink() }}" class="text-grey">{!! get_the_title() !!}</a></h2>
        <div class="entry-summary pusher-bottom">
            {!! App::excerpt(20) !!}
            <p class="pusher pusher-sm"><a href="{{ get_permalink() }}" class="cta white">VIEW POST</a></p>
        </div>
    </article>
</div>