<article {{ post_class() }}>
    @include('partials.blog-header')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @php the_content() @endphp
            </div>
        </div>
    </div>
</article>
<section class="bg-grey container-fluid">
    <div class="row">
        <div class="col">
            <h5 class="text-center text-primary">Latest news...</h5>
        </div>
    </div>
    <div class="row pusher pusher-sm">
        <div class="col">
            <div class="latest-posts">
                {!! $related_posts !!}
            </div>
        </div>
    </div>
</section>
