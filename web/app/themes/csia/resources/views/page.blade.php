@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @if(get_field('show_page_header'))
            @include('partials.page-header')
        @endif
    @endwhile
    {!! $sections !!}
@endsection
