@extends('layouts.app')

@section('content')
    @include('partials.page-header-no-content')
    <section class="container nopadding pusher-bottom pusher-bottom-sm">
        <div class="row">
            <div class="col">
                @if (!have_posts())
                    <div class="alert alert-warning">
                        {{ __('Sorry, no results were found.', 'sage') }}
                    </div>
                    {!! get_search_form(false) !!}
                @endif

                @while(have_posts()) @php the_post() @endphp
                    @include('partials.content-search')
                @endwhile

                {!! get_the_posts_navigation() !!}
            </div>
        </div>
    </section>
@endsection
