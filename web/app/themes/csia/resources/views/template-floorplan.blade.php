{{--
  Template Name: Floorplan
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')                
    @endwhile
    <section class="container-fluid nopadding-top">
        <div class="row">
            <div class="col">
                {!! do_shortcode('[pdfviewer width="100%" beta="true/false"]'.get_field('floorplan_upload').'[/pdfviewer]') !!}
            </div>
        </div>
    </section>
@endsection
