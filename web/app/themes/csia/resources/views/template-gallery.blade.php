{{--
  Template Name: Gallery
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')
        <section class="container nopadding">
            <div class="row gallery-container" id="gallery">
                <div class="col-12">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade show active" id="gallery-1">
                            <div class="row">
                                {!! $gallery['gallery'] !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="gallery-pagination-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#gallery-1" aria-controls="gallery-1" role="tab" data-toggle="tab" class="nav-link active">1</a>
                            </li>
                            {!! $gallery_pagination !!}
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    @endwhile
    <div id="gallery-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="slick-modal-gallery">
                    {!! $gallery['modal'] !!}
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
