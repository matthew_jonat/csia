{{--
  Template Name: Exhibitor List
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <p>
                        <a href="#" class="cta white full-width preventdefault adv-search">ADVANCED SEARCH</a>
                    </p>
                </div>
                <div class="col-12 col-sm-6">
                    <p>
                        <a href="#" class="cta white full-width preventdefault new-switch">NEW</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="exhibitor-search">
                        <form>
                            <div class="form-row">
                                <div class="col-8 col-sm-10">
                                    <input type="search" class="form-control search-term" placeholder="Search …" value="" name="s">
                                </div>
                                <div class="col-4 col-sm-2">
                                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            {!! $filter !!}
                        </div>
                        <div class="row pusher pusher-sm clear-search-container">
                            <div class="col">
                                <p>
                                    <a href="#" class="cta primary">CLEAR SEARCH</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pusher pusher-sm">
                <div class="col-12">
                    <div class="letters">
                        <span class="letter">A</span> <span class="break">|</span>
                        <span class="letter">B</span> <span class="break">|</span>
                        <span class="letter">C</span> <span class="break">|</span>
                        <span class="letter">D</span> <span class="break">|</span>
                        <span class="letter">E</span> <span class="break">|</span>
                        <span class="letter">F</span> <span class="break">|</span>
                        <span class="letter">G</span> <span class="break">|</span>
                        <span class="letter">H</span> <span class="break">|</span>
                        <span class="letter">I</span> <span class="break">|</span>
                        <span class="letter">J</span> <span class="break">|</span>
                        <span class="letter">K</span> <span class="break">|</span>
                        <span class="letter">L</span> <span class="break">|</span>
                        <span class="letter">M</span> <span class="break">|</span>
                        <span class="letter">N</span> <span class="break">|</span>
                        <span class="letter">O</span> <span class="break">|</span>
                        <span class="letter">P</span> <span class="break">|</span>
                        <span class="letter">Q</span> <span class="break">|</span>
                        <span class="letter">R</span> <span class="break">|</span>
                        <span class="letter">S</span> <span class="break">|</span>
                        <span class="letter">T</span> <span class="break">|</span>
                        <span class="letter">U</span> <span class="break">|</span>
                        <span class="letter">V</span> <span class="break">|</span>
                        <span class="letter">W</span> <span class="break">|</span>
                        <span class="letter">X</span> <span class="break">|</span>
                        <span class="letter">Y</span> <span class="break">|</span>
                        <span class="letter">Z</span>
                    </div>
                </div>
            </div>
            <div class="row pusher-more pusher-more-sm exhibitors-all">
                {!! $exhibitors !!}
            </div>
            <div class="row pusher-more pusher-more-sm exhibitors-new">
                {!! $new_exhibitors !!}
            </div>
        </div>
    @endwhile
    <div class="back-to-top">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 200 200" xml:space="preserve">
            <polygon points="155.86,0 40.78,0 0.11,0 0.11,40.16 0.11,155.24 44.87,200 44.87,44.76 200.62,44.76 "/>
        </svg>
    </div>
    <div class="modal fade info-modal exhibitor-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-modal-id="">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="featured-tri founding-partner">
                    <p>
                        <strong>FOUNDING</strong><br>
                        PARTNER
                    </p>
                </div>
                <div class="featured-tri must-see">
                    <div class="must-see-inside">
                        <p>
                            <strong>
                                MUST<br>
                                SEE!
                            </strong>
                        </p>
                    </div>
                </div>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary exit d-sm-none" data-dismiss="modal">
                    <polygon points="109.89,0.05 28.52,81.42 -0.24,110.18 28.16,138.58 109.53,219.95 172.84,219.95 63.07,110.18 173.2,0.05 "/>
                </svg>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary prev">
                    <polygon points="109.89,0.05 28.52,81.42 -0.24,110.18 28.16,138.58 109.53,219.95 172.84,219.95 63.07,110.18 173.2,0.05 "/>
                </svg>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <img src="" alt="" class="left-img img-fluid d-block mx-auto">
                    </div>
                    <div class="col-12 col-sm-8">
                        <p class="modal-info"></p>
                        <img src="" alt="" class="right-img img-fluid">
                        <div class="body"></div>
                        <p>
                            <span class="hidden-cta-website"><a href="" class="cta primary" target="_blank">VISIT WEBSITE</a></span> <span class="hidden-cta-email"><a href="" class="cta primary" target="_blank">GET IN TOUCH</a></span>
                        </p>
                        <p>
                            <a href="" target="_blank" class="social facebook"><i class="fa fa-facebook social-icon text-white"></i></a> <a href="" target="_blank" class="social instagram"><i class="fa fa-instagram social-icon text-white"></i></a> <a href="" target="_blank" class="social linkedin"><i class="fa fa-linkedin social-icon text-white"></i></a> <a href="" target="_blank" class="social twitter"><i class="fa fa-twitter social-icon text-white"></i></a>
                        </p>
                        <div class="row">
                            <div class="col-4">
                                <div class="cover-bg gal gal-0"></div>
                            </div>
                            <div class="col-4">
                                <div class="cover-bg gal gal-1"></div>
                            </div>
                            <div class="col-4">
                                <div class="cover-bg gal gal-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 173 220" xml:space="preserve" class="arrow primary next">
                    <polygon points="62.84,219.95 144.21,138.58 172.97,109.82 144.57,81.42 63.2,0.05 -0.1,0.05 109.66,109.82 -0.47,219.95 "/>
                </svg>
            </div>
        </div>
    </div>
@endsection

