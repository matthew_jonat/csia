@extends('layouts.app')

@section('content')
    @include('partials.blog-page-header')
    
    <section class="container nopadding pusher-bottom">
        <div class="row">
            <div class="col">
                <ul class="blog-categories">
                    {{ wp_list_categories(array(
                        'title_li' => '',
                        'exclude' => 1
                    )) }}
                    <li class="cat-item"><a href="/blog/">All Blogs</a></li>
                    <li class="cat-item search"><a href="#">Keyword Search</a></li>
                </ul>
                <div class="blog-search-form-container">
                    <form role="search" method="get" action="/">
                        <input type="hidden" name="post_type" value="post">
                        <div class="form-row">
                            <div class="col-8 col-sm-10">
                                <input type="search" class="form-control" placeholder="Search..." value="" name="s">
                            </div>
                            <div class="col-4 col-sm-2">
                                <button type="submit" class="btn btn-primary mb-2">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="container">
        <div class="row">
            @if (!have_posts())
                <div class="col">
                    <div class="alert alert-warning">
                        {{ __('Sorry, no results were found.', 'sage') }}
                    </div>
                    {!! get_search_form(false) !!}
                </div>
            @endif

            @while (have_posts()) @php the_post() @endphp
                @include('partials.content-'.get_post_type())
            @endwhile
        </div>
    </section>
    <section class="container nopadding">
        <div class="row">
            <div class="col">
                {!! get_the_posts_pagination([
                    'type' => 'list'
                ]) !!}
            </div>
        </div>
    </section>
@endsection
