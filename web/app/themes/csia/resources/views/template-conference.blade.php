{{--
  Template Name: Conference
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')
        <section class="container nopadding" id="conference">
            <div class="row">
                <div class="col">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="day1-tab" data-toggle="tab" href="#day1" role="tab" aria-controls="day1" aria-selected="true">Day 1</a>
                            <a class="nav-item nav-link" id="day2-tab" data-toggle="tab" href="#day2" role="tab" aria-controls="day2" aria-selected="false">Day 2</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="day1" data-speaker-count={{ $day1['totalSpeakers'] }} data-zero="0" role="tabpanel" aria-labelledby="day1-tab">
                            {!! $day1['sessions'] !!}
                        </div>
                        <div class="tab-pane fade" id="day2" role="tabpanel" data-speaker-count={{ $day2['totalSpeakers'] }} data-zero="99" aria-labelledby="day2-tab">
                            {!! $day2['sessions'] !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endwhile
    @include('partials.info-modal')
@endsection
