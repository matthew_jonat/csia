{{--
  Template Name: People
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
        @include('partials.page-header')
    @endwhile
    <section class="container nopadding">
        <div class="row people">
            {!! $people !!}
        </div>
    </section>
    @include('partials.info-modal')
@endsection
