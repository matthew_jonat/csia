import { jQuerySimpleCounter, createCookie, getCookie } from './functions';
import Instafeed from 'instafeed.js';

if($('.preventdefault').length > 0){
    $('.preventdefault').click(function(e){
        e.preventDefault();
    });
}

// Add slideDown animation to Bootstrap dropdown when expanding.
$('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
});

// Add slideUp animation to Bootstrap dropdown when collapsing.
$('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
});

// Use this to fix client access token
// https://instagram.com/oauth/authorize/?client_id=0528628820fb40849bbc6838e81c2e06&redirect_uri=https://cruiseshipinteriors-expo.com/&response_type=token
if($('#instafeed').length > 0){
    var feed = new Instafeed({
        clientId: '0528628820fb40849bbc6838e81c2e06',
        accessToken: '8050694290.0528628.0dbfa8d8d00849b4a113ffe0fc52f2eb',
        userId: '8050694290',
        get: 'user',
        template: '<div><div class="insta-post cover-bg" style="background-image: url({{image}})"><a href="{{link}}" target="_blank" class="cover-link"><i class="fa fa-instagram"></i></i></a></div></div>',
        limit: 4,
        resolution: 'standard_resolution',
        after: function(){
            $('#instafeed').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 1,
                infinite: false,
                arrows: false,
                mobileFirst: true,
                responsive: [
                    {
                        breakpoint: 540,
                        settings: 'unslick',
                    },
                ],
            })
        },
    });
    
    if($('#instafeed').length > 0){
        feed.run();
    }
}

if($('ul.blog-categories').length > 0){
    $('.cat-item.search').click(function(e){
        e.preventDefault();

        $(this).toggleClass('current-cat');

        $('.blog-search-form-container').slideToggle();
    });
}

if($('.info-modal').length > 0){
    $('body').keydown(function(e) {
        if(e.keyCode == 37) { // left
            $('.info-modal .modal-content svg.arrow.prev').trigger('click');
        }
        else if(e.keyCode == 39) { // right
            $('.info-modal .modal-content svg.arrow.next').trigger('click');
        }
    });
}

// For mobile devices, tap once and get hover state, second time goes to href.
if($('.tile-3').length > 0 && $(window).width() < 960){
    let countClick = [
        1,1,1,
    ];
    $('.tile-3 a.cover-link').click(function(e){
        e.preventDefault();

        let currentTileClick = $(this).parent('.tile-3').data('i');

        switch(countClick[currentTileClick]){
            case 1:
                $(this).parent('.tile-3').addClass('tapped');
                countClick[currentTileClick] = 2;
                break;
            case 2:
                window.location = $(this).attr('href');
                break;
        }
    });
}

$(window).resize(function(){
    $('.wrap').css('padding-top', $('header.banner').height() - 5);
});

// Give main content padding because of fixed header.
$(window).on('load', function(){
    $('.wrap').css('padding-top', $('header.banner').height() - 5);

    // Header scroll change.
    if($(window).width() > 960){
        let scrollUpOnce = false,
            scrollDownOnce = true,
            logoContainerHeight,
            runOnce = false;

        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if(scroll > 200){
                if(scrollDownOnce){
                    if(!runOnce){
                        logoContainerHeight = $('.logo-container').outerHeight();
                        runOnce = !runOnce;
                    }

                    $('.logo-container').height(logoContainerHeight);

                    $('header.banner').addClass('scroll');

                    $('.logo.init, .co-located-container').fadeOut(1, function(){
                        $('.logo.scroll-logo').fadeIn(1, function(){
                            $('.logo-container').animate({
                                height: $('.logo.scroll-logo img').height() + 30,
                            }, 100);
                        });
                    });
                    scrollDownOnce = !scrollDownOnce;
                    scrollUpOnce = !scrollUpOnce;
                }
            }else{
                if(scrollUpOnce){
                    $('header.banner').removeClass('scroll');

                    $('.logo.scroll-logo').fadeOut(1, function(){
                        $('.logo-container').animate({
                            height: logoContainerHeight,
                        }, 100, function(){
                            $('.logo.init, .co-located-container').fadeIn(1);
                        });
                        
                        
                    });
                    scrollUpOnce = !scrollUpOnce;
                    scrollDownOnce = !scrollDownOnce;
                }
                
            }
        });
    }

    if($('.eq-height').length > 0){
        let highest = 0;
        $('.eq-height').each(function(){
            if($(this).height() > highest){
                highest = $(this).height();
            }
        });
        $('.eq-height').height(highest);
    }

    // Stat counter section
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
        return elementBottom > viewportTop && elementTop < viewportBottom;
    };

    function loadStats(){
        $('.stat.amount').each(function(){
            jQuerySimpleCounter({
                start:  0,
                end: $(this).data('stat-amount'),
                duration: 3000,
            }, $(this));
        });
    }

    if($('.stat').length > 0){
        if($('.stat-container').isInViewport()){
            loadStats();
        }else{
            let runOnce = false;
            $(window).scroll(function(){
                if(!runOnce){
                    loadStats();
                    runOnce = true;
                }
            });
        }
    }

    if($('#register-modal').attr('data-show-modal') == '1' && !getCookie('registerModal')){
        setTimeout(function(){
            $('#register-modal').modal('show');
            createCookie('registerModal', true, 2);
        }, 5000);
    }
});