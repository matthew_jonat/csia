// Load more info modal
if($('.sponsup').length > 0){
    let infoModal = $('.info-modal'),
        totalSponsups = $('.sponsup').length;

    $('.sponsup').click(function(){
        let parent = $(this);
        
        infoModal.attr('data-modal-id', parent.data('id'));
        infoModal.find('img.left-img').attr('src', parent.find('.logo img').attr('src'));
        infoModal.find('p.modal-info').html(parent.find('.sponsup-info').html());
        infoModal.find('.right-img').hide();
        infoModal.find('.body').html(parent.find('.body').html());
        if(infoModal.find('span.boothno').text().length > 0){
            infoModal.find('span.boothno-container').css('display', 'block');
        }else{
            infoModal.find('span.boothno-container').css('display', 'none');
        }
        if(parent.data('url').length > 0){
            infoModal.find('.hidden-cta a').attr('href', parent.data('url'));
            infoModal.find('.hidden-cta').css('display', 'block');
        }else{
            infoModal.find('.hidden-cta').css('display', 'none');
        }
        infoModal.modal('show');
    });

    // Show next
    $('.arrow.next').click(function(){
        let current = infoModal.attr('data-modal-id'),
            nextID = parseInt(current) + 1,
            nextEl = $('[data-id='+nextID+']'),
            logo = nextEl.find('.logo img').attr('src'),
            info = nextEl.find('.sponsup-info').html(),
            body = nextEl.find('.body').html(),
            link = nextEl.data('url');

        if(nextID <= totalSponsups){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', logo);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('.body').html(body);
            if(infoModal.find('span.boothno').text().length > 0){
                infoModal.find('span.boothno-container').css('display', 'block');
            }else{
                infoModal.find('span.boothno-container').css('display', 'none');
            }
            if(link.length > 0){
                infoModal.find('.hidden-cta a').attr('href', link);
                infoModal.find('.hidden-cta').css('display', 'block');
            }else{
                infoModal.find('.hidden-cta').css('display', 'none');
            }
        }
    });

    // Show next
    $('.arrow.prev').click(function(){
        let current = infoModal.attr('data-modal-id'),
            nextID = parseInt(current) - 1,
            nextEl = $('[data-id='+nextID+']'),
            logo = nextEl.find('.logo img').attr('src'),
            info = nextEl.find('.sponsup-info').html(),
            body = nextEl.find('.body').html(),
            link = nextEl.data('url');

        if(nextID > 0){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', logo);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('.body').html(body);
            if(infoModal.find('span.boothno').text().length > 0){
                infoModal.find('span.boothno-container').css('display', 'block');
            }else{
                infoModal.find('span.boothno-container').css('display', 'none');
            }
            if(link.length > 0){
                infoModal.find('.hidden-cta a').attr('href', link);
                infoModal.find('.hidden-cta').css('display', 'block');
            }else{
                infoModal.find('.hidden-cta').css('display', 'none');
            }
        }
    });
}
