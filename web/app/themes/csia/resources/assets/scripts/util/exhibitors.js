import { showHideExhibModal } from './functions';

if($('.exhibitors-all').length > 0){

    // Show / hide search options
    $('.adv-search').click(function(){
        $(this).toggleClass('white primary active');
        $('.exhibitor-search').slideToggle();
    });

    // Show / hide exhibitors by checking filters.
    let selected_activities = [];
    $('.filter-check').on('change', function(){
        $('.exhibitor-container').hide();

        let activity = $(this).val();
        
        if($(this).is(':checked')){
            if($.inArray(activity, selected_activities) == -1){
                selected_activities.push(activity);
            }
        }else{
            selected_activities = selected_activities.filter(el => el !== activity);  
        }

        if(selected_activities.length > 0){
            $('.exhibitor').each(function(){
                let filters = $(this).attr('data-filters').split(', ');
                
                filters.forEach(e1 => {
                    selected_activities.forEach(e2 => {
                        if(e1 === e2){
                            $(this).parent('.exhibitor-container').show();
                            return;
                        }
                    })
                })
            });
        }else{
            $('.exhibitor-container').show();
        }
    });

    // Switch between new and all exhibitors
    $('.new-switch').click(function(){
        if($(this).hasClass('switched')){
            $('.exhibitors-new').fadeOut(300, function(){
                $('.letters').slideDown();
                $('.exhibitors-all').css('display', 'flex').hide().fadeIn();
            });
        }else{
            $('.exhibitors-all').fadeOut(300, function(){
                $('.letters').slideUp();
                $('.exhibitors-new').css('display', 'flex').hide().fadeIn();
            });    
        }
        $(this).toggleClass('primary switched active');
    });

    // Load more info modal
    let infoModal = $('.exhibitor-modal'),
        totalExhibitors = $('.exhibitors-all .exhibitor').length;

    $('.exhibitor').click(function(){
        let parent = $(this);

        if(parent.data('founding-partner')){
            infoModal.find('.featured-tri.founding-partner').css('display', 'block');
        }else{
            infoModal.find('.featured-tri.founding-partner').css('display', 'none');
        }
        
        if(parent.data('must-see')){
            infoModal.find('.featured-tri.must-see').css('display', 'block');
        }else{
            infoModal.find('.featured-tri.must-see').css('display', 'none');
        }

        infoModal.attr('data-modal-id', parent.data('id'));
        infoModal.find('img.left-img').attr('src', parent.data('logo'));
        infoModal.find('p.modal-info').html(parent.find('.exhibitor-info').html());
        infoModal.find('.right-img').hide();
        infoModal.find('.body').html(parent.find('.body-full').html());
        showHideExhibModal(infoModal, parent.data('url'), parent.data('url'), '.hidden-cta-website a', '.hidden-cta-website'); // Show / hide website CTA
        showHideExhibModal(infoModal, parent.data('email'), 'mailto:' + parent.data('email'), '.hidden-cta-email a', '.hidden-cta-email'); // Show / hide email CTA
        showHideExhibModal(infoModal, parent.data('url-facebook'), parent.data('url-facebook'), '.social.facebook', '.social.facebook') // Show / hide facebook social icon
        showHideExhibModal(infoModal, parent.data('url-instagram'), parent.data('url-instagram'), '.social.instagram', '.social.instagram') // Show / hide instagram social icon
        showHideExhibModal(infoModal, parent.data('url-linkedin'), parent.data('url-linkedin'), '.social.linkedin', '.social.linkedin') // Show / hide linkedin social icon
        showHideExhibModal(infoModal, parent.data('url-twitter'), parent.data('url-twitter'), '.social.twitter', '.social.twitter') // Show / hide twitter social icon

        let images = parent.data('gallery').split(',');

        if(images[0].length > 0){
            images.forEach((val, i) => {
                infoModal.find('.gal.gal-'+i).css({
                    'background-image': 'url(' + val + ')',
                    'display': 'block',
                });
            });
        }else{
            infoModal.find('.gal').css('display', 'none');
        }

        $('svg.arrow').css('display', 'block');
        infoModal.modal('show');
    });

    // Show next
    $('.arrow.next').click(function(){
        let current = infoModal.attr('data-modal-id'),
            nextID = parseInt(current) + 1,
            nextEl = $('[data-id='+nextID+']'),
            foundingPartner = nextEl.data('founding-partner'),
            mustSee = nextEl.data('must-see'),
            logo = nextEl.data('logo'),
            link = nextEl.data('url'),
            email = nextEl.data('email'),
            facebook = nextEl.data('url-facebook'),
            instagram = nextEl.data('url-instagram'),
            linkedin = nextEl.data('url-linkedin'),
            twitter = nextEl.data('url-twitter'),
            images = nextEl.data('gallery').split(','),
            info = nextEl.find('.exhibitor-info').html(),
            body = nextEl.find('.body-full').html();

        if(images[0].length > 0){
            images.forEach((val, i) => {
                infoModal.find('.gal.gal-'+i).css({
                    'background-image': 'url(' + val + ')',
                    'display': 'block',
                });
            });
        }else{
            infoModal.find('.gal').css('display', 'none');
        }

        if(nextID <= totalExhibitors){
            if(foundingPartner){
                infoModal.find('.featured-tri.founding-partner').css('display', 'block');
            }else{
                infoModal.find('.featured-tri.founding-partner').css('display', 'none');
            }

            if(mustSee){
                infoModal.find('.featured-tri.must-see').css('display', 'block');
            }else{
                infoModal.find('.featured-tri.must-see').css('display', 'none');
            }

            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', logo);
            showHideExhibModal(infoModal, link, link, '.hidden-cta-website a', '.hidden-cta-website'); // Show / hide website CTA
            showHideExhibModal(infoModal, email, 'mailto:' + email, '.hidden-cta-email a', '.hidden-cta-email'); // Show / hide email CTA
            showHideExhibModal(infoModal, facebook, facebook, '.social.facebook', '.social.facebook'); // Show / hide facebook social icon
            showHideExhibModal(infoModal, instagram, instagram, '.social.instagram', '.social.instagram'); // Show / hide instagram social icon
            showHideExhibModal(infoModal, linkedin, linkedin, '.social.linkedin', '.social.linkedin'); // Show / hide linkedin social icon
            showHideExhibModal(infoModal, twitter, twitter, '.social.twitter', '.social.twitter'); // Show / hide twitter social icon
            infoModal.find('p.modal-info').html(info);
            infoModal.find('.body').html(body);
        }
    });

    // Show previous
    $('.arrow.prev').click(function(){
        let current = infoModal.attr('data-modal-id'),
            nextID = parseInt(current) - 1,
            nextEl = $('[data-id='+nextID+']'),
            foundingPartner = nextEl.data('founding-partner'),
            mustSee = nextEl.data('must-see'),
            logo = nextEl.data('logo'),
            link = nextEl.data('url'),
            email = nextEl.data('email'),
            facebook = nextEl.data('url-facebook'),
            instagram = nextEl.data('url-instagram'),
            linkedin = nextEl.data('url-linkedin'),
            twitter = nextEl.data('url-twitter'),
            images = nextEl.data('gallery').split(','),
            info = nextEl.find('.exhibitor-info').html(),
            body = nextEl.find('.body-full').html();

        if(images[0].length > 0){
            images.forEach((val, i) => {
                infoModal.find('.gal.gal-'+i).css({
                    'background-image': 'url(' + val + ')',
                    'display': 'block',
                });
            });
        }else{
            infoModal.find('.gal').css('display', 'none');
        }

        if(nextID > 0){
            if(foundingPartner){
                infoModal.find('.featured-tri.founding-partner').css('display', 'block');
            }else{
                infoModal.find('.featured-tri.founding-partner').css('display', 'none');
            }

            if(mustSee){
                infoModal.find('.featured-tri.must-see').css('display', 'block');
            }else{
                infoModal.find('.featured-tri.must-see').css('display', 'none');
            }

            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', logo);
            showHideExhibModal(infoModal, link, link, '.hidden-cta-website a', '.hidden-cta-website'); // Show / hide website CTA
            showHideExhibModal(infoModal, email, 'mailto:' + email, '.hidden-cta-email a', '.hidden-cta-email'); // Show / hide email CTA
            showHideExhibModal(infoModal, facebook, facebook, '.social.facebook', '.social.facebook'); // Show / hide facebook social icon
            showHideExhibModal(infoModal, instagram, instagram, '.social.instagram', '.social.instagram'); // Show / hide instagram social icon
            showHideExhibModal(infoModal, linkedin, linkedin, '.social.linkedin', '.social.linkedin'); // Show / hide linkedin social icon
            showHideExhibModal(infoModal, twitter, twitter, '.social.twitter', '.social.twitter'); // Show / hide twitter social icon
            infoModal.find('p.modal-info').html(info);
            infoModal.find('.body').html(body);
        }
    });

    //Search form
    $('.exhibitor-search button').click(function(e){
        e.preventDefault();

        let searchTerm = $('input.search-term').val().toLowerCase();

        $('.exhibitor-container').hide();

        $('.clear-search-container').slideDown();

        $('.exhibitor').each(function(){
            let t = $(this);
            if(t.find('.exhibitor-info').text().toLowerCase().includes(searchTerm) || t.find('.body').text().toLowerCase().includes(searchTerm)){
                t.parent().show();
            }
        });
    });

    $('.clear-search-container a').click(function(e){
        e.preventDefault();

        $('input.search-term').val('');

        $('.exhibitor-container').show();

        $('.clear-search-container').slideUp();
    });

    // Alphabet
    let letters = [];
    $('.exhibitor').each(function(){
        if($(this).attr('id') != ''){
            letters.push($(this).attr('id'));
        }
    });

    $('.letter').each(function(){
        if($.inArray($(this).html().trim(), letters) >= 0){
            $(this).addClass('active');
        }
    });

    $('.letter').click(function(){
        var currentLetter = $(this).html();
        $('html, body').animate({
            scrollTop: $('#'+currentLetter.trim()).offset().top - 180,
        }, 1000);
    });

    // Back to top
    $(document).scroll(function() {
        if($(document).scrollTop() > 600){
            $('.back-to-top').addClass('active');
        }else{
            $('.back-to-top').removeClass('active');
        }
    });

    $('.back-to-top').click(function(){
        $('html, body').animate({
            scrollTop: 0,
        }, 1000);
    });
}