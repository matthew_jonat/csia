if($('.person').length > 0){
    let infoModal = $('.info-modal'),
        totalPeople = $('.person').length;

    //Change headshot to logo on hover
    $('.open-people-modal').on('mouseenter', function(){
        var src = $(this).parent().data('logo-url');
        var headshotSrc = $(this).parent().find('img').attr('src');
        var currentHeight = $(this).parent().find('img').height();
        console.log(currentHeight);
        $(this).parent().find('img').css('height', currentHeight);
         $(this).parent().find('img').css('object-fit', 'contain');
        $(this).parent().find('img').attr('src', src);
        $(this).data('headshot-url', headshotSrc);
    });

    // Change back on leave
    $('.open-people-modal').on('mouseleave', function(){
        var src = $(this).data('headshot-url');
        $(this).parent().find('img').attr('src', src);
        $(this).parent().find('img').css('object-fit', 'unset');
    });

    $('.open-people-modal').click(function(e){
        e.preventDefault();
        let parent = $(this).parents('.person');

        infoModal.attr('data-modal-id', parent.data('id'));
        infoModal.find('img.left-img').attr('src', parent.find('.headshot img').attr('src'));
        infoModal.find('p.modal-info').html(parent.find('.person-info').html());
        infoModal.find('img.right-img').attr('src', parent.data('logo-url'));
        infoModal.find('.body').html(parent.find('.body').html());
        infoModal.modal('show');
    });

    // Show next
    $('.arrow.next').click(function(){
        let current = $('.info-modal').attr('data-modal-id'),
            nextID = parseInt(current) + 1,
            nextEl = $('[data-id='+nextID+']'),
            headshot = nextEl.find('.headshot img').attr('src'),
            info = nextEl.find('.person-info').html(),
            logo = nextEl.data('logo-url'),
            body = nextEl.find('.body').html();

        if(nextID <= totalPeople){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', headshot);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('img.right-img').attr('src', logo);
            infoModal.find('.body').html(body);
        }
    });

    // Show previous
    $('.arrow.prev').click(function(){
        let current = $('.info-modal').attr('data-modal-id'),
            nextID = parseInt(current) - 1,
            nextEl = $('[data-id='+nextID+']'),
            headshot = nextEl.find('.headshot img').attr('src'),
            info = nextEl.find('.person-info').html(),
            logo = nextEl.data('logo-url'),
            body = nextEl.find('.body').html();

        if(nextID > 0){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', headshot);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('img.right-img').attr('src', logo);
            infoModal.find('.body').html(body);
        }
    });
}