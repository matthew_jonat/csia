if($('.session').length > 0){
    // Show / hide session
    $('.session-head').click(function(){
        let session = $(this).parents('.session');
    
        session.toggleClass('active');
        if($(window).width() > 768){
            session.find('span.timeto').slideToggle();
        }
        session.find('.session-body').slideToggle();
    });

    // Modal
    let infoModal = $('.info-modal');

    $('.speaker').click(function(){
        let parent = $(this);

        infoModal.attr('data-modal-id', parent.data('id'));
        infoModal.find('img.left-img').attr('src', parent.find('img.headshot').attr('src'));
        infoModal.find('p.modal-info').html(parent.find('.person-info').html());
        infoModal.find('img.right-img').attr('src', parent.find('img.logo').attr('src'));
        infoModal.find('.body').html(parent.find('.body').html());
        infoModal.modal('show');
    });

    // Show next
    $('.arrow.next').click(function(){
        let current = $('.info-modal').attr('data-modal-id'),
            nextID = parseInt(current) + 1,
            nextEl = $('[data-id='+nextID+']'),
            headshot = nextEl.find('img.headshot').attr('src'),
            info = nextEl.find('.person-info').html(),
            logo = nextEl.find('img.logo').attr('src'),
            body = nextEl.find('.body').html(),
            totalSpeakers = $('[data-id='+current+']').parents('.tab-pane').data('speaker-count');

        if(nextID <= totalSpeakers){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', headshot);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('img.right-img').attr('src', logo);
            infoModal.find('.body').html(body);
        }
    });

    // Show previous
    $('.arrow.prev').click(function(){
        let current = $('.info-modal').attr('data-modal-id'),
            nextID = parseInt(current) - 1,
            nextEl = $('[data-id='+nextID+']'),
            headshot = nextEl.find('img.headshot').attr('src'),
            info = nextEl.find('.person-info').html(),
            logo = nextEl.find('img.logo').attr('src'),
            body = nextEl.find('.body').html();

        if(nextID > $('[data-id='+current+']').parents('.tab-pane').data('zero')){
            infoModal.attr('data-modal-id', nextID);
            infoModal.find('img.left-img').attr('src', headshot);
            infoModal.find('p.modal-info').html(info);
            infoModal.find('img.right-img').attr('src', logo);
            infoModal.find('.body').html(body);
        }
    });
}