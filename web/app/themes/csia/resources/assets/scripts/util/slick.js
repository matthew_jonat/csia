import 'slick-carousel';

if($('.slick-3-tile').length > 0){
    $('.slick-3-tile').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        arrows: false,
        initialSlide: 1,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 540,
                settings: 'unslick',
            },
        ],
    });
}

if($('.logos-container').length > 0){
    $('.logos-container').slick({
        autoplay: true,
        autoplaySpeed: 0,
        arrows: false,
        cssEase: 'linear',
        draggable: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 3000,
        swipe: false,
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 5,
                },
            },
        ],
    });
}

if($('.testimonials').length > 0){
    $('.testimonials').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        dots: true,
        appendDots: $('.testimonial-dots'),
    })
}

if($('.latest-posts').length > 0){
    $('.latest-posts').slick({
        arrows: false,
        infinite: false,
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    centerMode: false,
                    slidesToShow: 3,
                },
            },
            {
                breakpoint: 1140,
                settings: {
                    centerMode: false,
                    slidesToShow: 5,
                },
            },
        ],
    });
}

if($('.half-img-slides').length > 0){
    $('.half-img-slides').slick({
        autoplay: true,
        arrows: false,
        vertical: true,
    });
}