if($('.gallery-container').length > 0){
    $('.slick-modal-gallery').slick({
        arrows: false,
        mobileFirst: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    prevArrow: '<svg x="0px" y="0px" viewBox="0 0 173 220" class="slick-prev arrow primary"><polygon points="109.89,0.05 28.52,81.42 -0.24,110.18 28.16,138.58 109.53,219.95 172.84,219.95 63.07,110.18 173.2,0.05 "/></svg>',
                    nextArrow: '<svg x="0px" y="0px" viewBox="0 0 173 220" class="slick-next arrow primary"><polygon points="62.84,219.95 144.21,138.58 172.97,109.82 144.57,81.42 63.2,0.05 -0.1,0.05 109.66,109.82 -0.47,219.95 "/></svg>',
                },
            },
        ],
    });

    $('.gallery-image').click(function(){
        let goTo = $(this).data('pos');

        $('.slick-modal-gallery').slick('slickGoTo', goTo);
        $('#gallery-modal').modal('show');
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(){
        $('html, body').animate({
            scrollTop: $('.gallery-container').offset().top - 200,
        }, 1000);
    });
}